Front:
    - React TypeScript OK
    - Movie Details
    - Fetch movies from BackEnd API


Back:
    - Express node
    - MongoDB favorites
    - Return List of Trending movies
    - Return favorites


Docs:

Trending movies
https://developers.themoviedb.org/3/trending/get-trending

Base URL Image
https://image.tmdb.org/t/p/w500/


