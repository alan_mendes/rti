import React, {useState, useEffect} from 'react';
import arrow from '../Header/arrow.png'
import './MovieDetailsCss.css'

const MovieDetails = ({moreDetails, modalStatus}: any) => {

    const [isOpen, setIsOpen] = useState(false)

    const isModalOpen = () => {
        setIsOpen(!isOpen)
    }

    console.log('modalStatus => ', modalStatus, 'isOpen => ', isOpen);


    useEffect( () => {

        setIsOpen(modalStatus)

      }, [modalStatus])

  return isOpen ? (
    <div className="MovieDetails">
        <div className="App-header">
            <div className="App-Header__line">
                {(
                    <div className="App-Header__returnBtn">
                    <div onClick={isModalOpen}>
                        <img src={arrow} alt=""/>
                    </div>
                    </div>)
                }
                <p className="App-Header__text">
                {moreDetails.title ? moreDetails.title : moreDetails.name }
                </p>
            </div>
        </div>

        <div  className="MovieDetails__block1">
            <div>
                <img className="MovieDetails__picture" src={`https://image.tmdb.org/t/p/w500/${moreDetails.poster_path}`} alt=""/>
            </div>

            <div className="MovieDetails__block1__description">
                    <p className="MovieDetails__block1__title">{moreDetails.title ? moreDetails.title : moreDetails.name}</p>
                    <p className="MovieDetails__block1__date">{moreDetails.release_date ? moreDetails.release_date : moreDetails.first_air_date}</p>
                    <p className="MovieDetails__block1__vote">Vote Avarage: {moreDetails.vote_average}</p>
            </div>
        </div>

        <div className="MovieDetails__block2">
                <p>
                    {moreDetails.overview}
                </p>
        </div>

    </div>
  ): null


}

export default MovieDetails;
