import React, { useEffect, useState } from 'react';
import axios from 'axios'
import CatalogGrid from '../CatalogGrid/CatalogGrid'
import './HomeCss.css'

const Home = () => {

  const [trending, setTrending] = useState({})

  useEffect( () => {

    const fetchTrendingMovies = async () => {
      const { data: {results} } = await axios.get('http://localhost:8000/trending')

      setTrending(results)
    }

    fetchTrendingMovies()

  }, [])

  return (
    <div className="Home">
        <CatalogGrid
          movies={trending}
        />
    </div>
  );
}

export default Home;
