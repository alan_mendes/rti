import React from 'react';
import './HeaderCss.css'

export interface HeaderInerface {
  title: string
}

const  Header = ({title}: HeaderInerface) => {

  return (
    <header className="App-header">
      <div className="App-Header__line">
          <p className="App-Header__text">
            {title }
          </p>
      </div>
    </header>
  );
}

export default Header;
