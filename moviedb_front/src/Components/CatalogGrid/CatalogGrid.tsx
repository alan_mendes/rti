import React, { useState } from 'react';
import './CatalogGridCss.css'
import MovieDetails from '../MovieDetails/MovieDetails'

const CatalogGrid = ({movies}: any) => {

    const [details, setDetails] = useState({})
    const [isModalOpen, SetModal] = useState(false)

    const fetchDetails = (movie: any) => {
        return (event: React.MouseEvent) => {

            setDetails(movie)
            SetModal(!isModalOpen)
            event.preventDefault();
          }
    }

  return (
    <div className="CatalogGrid">

        <MovieDetails
            moreDetails={details}
            modalStatus={isModalOpen}
        />

        {
            Object.keys(movies).map(key => {
                return (
                    // eslint-disable-next-line
                    <a  className="CatalogGrid__item" onClick={fetchDetails(movies[key])}>
                        <img className="CatalogGrid__item__img" src={`https://image.tmdb.org/t/p/w500/${movies[key].poster_path}`} alt=""/>
                    </a>

                )
            })
        }
    </div>
  )
}

export default CatalogGrid;
