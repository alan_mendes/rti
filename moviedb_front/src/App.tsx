import React from 'react';
import Header  from './Components/Header/Header'
import Home  from './Components/Home/Home'
import './App.css';

const App = () => {
  return (
    <div className="App">
      <div>
        <Header
          title = { 'Top Movies' }
        />
      </div>
      <div>
        <Home />
      </div>
    </div>
  );
}

export default App;
