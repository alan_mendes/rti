
var express = require('express')
const app = express()


let indexRoute = require("./routes/index")
let trendingRoute = require("./routes/trending")

app.use("/", indexRoute)
app.use("/trending", trendingRoute)

app.listen(8000)

module.exports = app