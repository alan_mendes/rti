
let Express = require("express")
let Router = Express.Router();

Router.get("/", function (req, res, next) {
    res.set({'Access-Control-Allow-Origin': '*'})
    res.send("Index Page Controller :D")
});

module.exports = Router;