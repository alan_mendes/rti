require('dotenv').config();
const axios = require('axios').default;

const fetchTrending = async () => {
    return await axios.get(`https://api.themoviedb.org/3/trending/all/week?api_key=${process.env.API_KEY}`)
}

console.log(process.env.API_KEY);

let Express = require("express")
let Router = Express.Router();

Router.get("/", async function (req, res, next) {
    res.set({'Access-Control-Allow-Origin': '*'})
    const {data} = await fetchTrending()

    res.send(data)
});

module.exports = Router;